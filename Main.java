package com.zuitt;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Phonebook phoneBook = new Phonebook();

        while (true) {
            // Get user input for contact
            System.out.println("Enter name for new contact (or 'quit' to exit):");
            String name = scanner.nextLine();

            // Check if user wants to quit
            if (name.equalsIgnoreCase("quit")) {
                break;
            }
            System.out.println("Enter phone number for new contact:");
            String contactNumber = scanner.nextLine();

            ArrayList<String> addresses = new ArrayList<>();

            System.out.println("Enter home address for new contact:");
            String homeAddress = scanner.nextLine();
            addresses.add(homeAddress);

            System.out.println("Enter work address for new contact:");
            String workAddress = scanner.nextLine();
            addresses.add(workAddress);

            Contact contact = new Contact(name, contactNumber, addresses);

            phoneBook.addContact(contact);
        }

        if (phoneBook.getContacts().isEmpty()) {
            System.out.println("Phone book is empty.");
        } else {
            System.out.println("Phone book contains the following contacts:");
            for (Contact newContact : phoneBook.getContacts()) {
                System.out.println(newContact.getName() + " has the following registered numbers:");
                System.out.println("Phone Number: " + newContact.getContactNumber());
                System.out.println("-".repeat(20));
                System.out.println(newContact.getName() + " has the following registered addresses:");
                ArrayList<String> newContactAddresses = newContact.getAddresses();
                for (int i = 0; i < newContactAddresses.size(); i++) {
                    String address = newContactAddresses.get(i);
                    if (i == 0) {
                        System.out.println("My home is in " + address);
                    } else if (i == 1) {
                        System.out.println("My office is in " + address);
                    }
                }
                System.out.println("=".repeat(20));
            }
        }
    }
}


