package com.zuitt;

import java.util.ArrayList;

public class Phonebook {
    private ArrayList<Contact> contacts;

    // Default constructor
    public Phonebook() {
        contacts = new ArrayList<>();
    }

    // Parameterized constructor
    public Phonebook(ArrayList<Contact> contacts) {
        this.contacts = contacts;
    }

    // Getter for contacts
    public ArrayList<Contact> getContacts() {
        return contacts;
    }

    // Setter for contacts
    public void setContacts(ArrayList<Contact> contacts) {
        this.contacts = contacts;
    }

    // Method to add a contact to the contacts list
    public void addContact(Contact contact) {
        contacts.add(contact);
    }

}
