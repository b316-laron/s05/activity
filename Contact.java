package com.zuitt;

import java.util.ArrayList;

public class Contact {
    private String name;
    private String contactNumber;
    private ArrayList<String> addresses;

    public Contact() {
        addresses = new ArrayList<>();
    }

    public Contact(String name, String contactNumber, String address) {
        this.name = name;
        this.contactNumber = contactNumber;
        this.addresses = new ArrayList<>();
        this.addresses.add(address);
    }

    public Contact(String name, String contactNumber, ArrayList<String> addresses) {
        this.name = name;
        this.contactNumber = contactNumber;
        this.addresses = addresses;
    }

    public String getName() {
        return this.name;
    }

    public String getContactNumber() {
        return this.contactNumber;
    }

    public ArrayList<String> getAddresses() {
        return this.addresses;
    }

    public void addAddress(String address) {
        this.addresses.add(address);
    }
}
